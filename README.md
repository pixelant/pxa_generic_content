# [pxa_generic_content](https://bitbucket.org/pixelant/pxa_generic_content) **0.0.3-dev**


[**Contributing**](#markdown-header-contributing)

[**Changelog**](#markdown-header-changelog)

#### Git instructions

[Git workflow](https://docs.google.com/a/pixelant.se/document/d/1gVPXDCBrR66Ahhh7c6BMBeQZ0KpWWxhlLEw6zNOZX1g/edit?usp=sharing)

[Semantic Versioning](http://semver.org/)

***

# Contributing

Everyone can leave feedback, bug reports and even better - pull requests. Here is several rules for contribute this repository.  Please keep this in mind for better cooperation.

### Issues

If you have a question not covered in the documentation or want to report a bug, the best way to ensure it gets addressed is to file it in the appropriate issues tracker.

* Used the search feature to ensure that the bug hasn't been reported before
* Try to reduce your code to the bare minimum required to reproduce the issue. This makes it much easier (and much faster) to isolate and fix the issue.


### Pull requests

If you want to fix bug by yourself or add new features, you can use pull requests.

* One repository:

    1. Clone repository `git clone git@bitbucket.org:pixelant/felayout_ricky.git`
    2. Checkout to branch `develop` `git checkout -t origin/develop`
    3. Create new branch from `develop` `git checkout -b branchName`
    4. Add your changes, and commit it.
    5. Push your branch to repository `git push origin branchName`
    6. Create pull request on Bitbucket

* Please check to make sure that there aren't existing pull requests attempting to address the issue mentioned. Also recommend checking for issues, maybe somebody working with the same issue.
* Non-trivial changes should be discussed in an issue first.
* Lint the code by running `grunt check`.
* Dont forget about [editorconfig](http://editorconfig.org/).

***

***
#Changelog
***
* [FEATURE] New "dynamic icon" selector in fluidcontent flexforms.
* [TASK] Separate elements with icons into two separate content elements.
* [TASK] Add 2,3 and 4 column content elements on Generic tab.
* [TASK] Move Logo, Social Icons to Special tab.
* [FEATURE] Added parallax content element.
* [TASK] Updated templates with grid (container). Moved label and duplicated name to flex.column from flex.content.
* [FEATURE] Added condition, don't display socialicon if there is no link.
* [FIX] Fixed label pointing wrong.
* [FEATURE] Added types for fluidcontent tabs.
* [FEATURE] Removed static labels, changed links to 'typolink'.
* [FEATURE] Added labels for socialIcons.
* [FEATURE] Added types for fluidcontent socialIcons.
* [FEATURE] Removed static labels, changed link to 'typolink'.
* [FEATURE] Added labels for quote.
* [FEATURE] Added types for fluidcontent quote.
* [FEATURE] Changed so images and header is from tt_content.
* [FEATURE] Added labels for logoCarousel.
* [FEATURE] Added types for fluidcontent logoCarousel.
* [FEATURE] Changed so logo uses first image from tt_content, optionally linked, and alttext if written, else site name from GeneralUtility::getIndpEnv('TYPO3SITEURL').
* [FEATURE] Added types for fluidcontent logo.
* [FEATURE] Renamed sheet + section + object to unify it between fluidcontent templates. Changed way of force save one time first.
* [FIX] Fixed label paths, added labels.
* [FEATURE] Added types for fluidcontent grid.
* [FEATURE] Added tab for options. Moved render options there so grid is in separate tab.
* [FIX] Fixed label paths, added label.
* [FIX] Removed labels in template.
* [FEATURE] Added labels for some fields in advGrid.
* [FEATURE] Added types for fluidcontent carousel.
* [FEATURE] Added labels for new fields in contact form. Renamed label in carousel.
* [FEATURE] Added types for fluidcontent contact.
* [FEATURE] Added fields for labels. Generate email as link so it is encrypted. Removed labels in template.
* [UPDATE] Updated language labels paths.
* [FEATURE] Renamed sheet + section + object to unify it between fluidcontent templates. Changed way of force save one time first.
* [FEATURE] Added types for advgrid.
* [UPDATE] Updated language labels paths.
* [FEATURE] Renamed sheet + section + object to unify it between fluidcontent templates.
* [FEATURE] Renamed sheet + section + object to unify it between fluidcontent templates. Changed way of force save one time first.
* [FEATURE] Changed object name to children/child, dont display real tab until record is saved, now uses unique id:s for content areas.
* [FEATURE] Added label for use in types override for header not visible in frontend. Updated labels for accordion.
* [FEATURE] Added types override for accordion.
* [FIX] Removed labels in fields.
* [FIX] Added translations for element 1.
* [FIX] Removed test output in Preview section.
* [FEATURE] Changed so fluidcontent uses some fields from tt_content.
* [FEATURE] Added types override for fluidcontent 1.
* [FEATURE] Changed linkhandler so more linktypes are supported.
* [FIX] Added comments.
* [FEATURE] Added file for overriding tt_content TCA settings. Add custom palettes and 'overrides' of 'fluidcontent_content' types here.
* [FEATURE] Don't show second tab unless 'mode' is selected. By doing this we can force the content to be saved before we add 'tabs/pills'. Then each tab/pill object will be stored with a unique id, which now is used to target container. This will fix the problem that the tabs/pills content didn't follow its original parent when reordering tabs/pills in form.
* [FEATURE] Removed some inputs, changed so output is rendered from tt_content fields for the removed fields instead.
* [FEATURE] Added hook so we can override the palettes for types 'fluidcontent_content' dynamically.
* [UPDATE] Added som labels.
* [FEATURE] Remove standard header output for fluidcontent so we can handle it from our fluidcontent.
* fix grid
* add logoCarousel, quote, modify carousel

# **0.0.2**
