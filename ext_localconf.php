<?php

if (!defined ('TYPO3_MODE'))
	die('Access denied.');

    \FluidTYPO3\Flux\Core::registerProviderExtensionKey('pxa_generic_content', 'Content');
    // Page module hook
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tceforms.php']['getMainFieldsClass']['pxa_generic_content'] = 'Pixelant\PxaGenericContent\Hook\FormEngine';

?>
