function updateSelection(index, itemFormElName, table, id, field, name) {

    TYPO3.jQuery('[name="' + itemFormElName + '_hr"]').val(name);
    TYPO3.jQuery('[name="' + itemFormElName + '"]').val(name);
    TYPO3.jQuery('[name="table-' + itemFormElName + '"] td').removeClass('current');
    TYPO3.jQuery('[name="table-' + itemFormElName + '"] #selIcon-' + index).addClass('current');

}
