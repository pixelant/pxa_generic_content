<?php
namespace Pixelant\PxaGenericContent\Hook;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Mats Svensson <mats@pixelant.se>, Pixelant AB
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * 'FormEngine' - Class for hooks from TYPO3\CMS\Backend\Form\FormEngine.
 *
 * Provides methods to change various TCA configurations related
 * to Fluid Content Elements.
 *
 * @author Mats Svensson, Pixelant AB
 * @package pxa_generic_content
 * @subpackage Hook
 */
class FormEngine {
    /**
     * Hook called from TYPO3\CMS\Backend\Form\FormEngine::getMainFields
     * If table is tt_content and field tx_fed_fcefile isn't empty
     * check if there is a configuration in TCA for types with the name of the fce_file
     * , then replaces original types 'fluidcontent_content' showitem with this.
     *
     * @param string $table The table name
     * @param array $row The record from the table for which to render a field.
     * @param TYPO3\CMS\Backend\Form\FormEngine $pObj The object calling the hook.
     */
    public function getMainFields_preProcess($table, array $row, $pObj) {

            // If table is tt_content and field tx_fed_fcefile isn't empty
        if ($table == 'tt_content' && $row['tx_fed_fcefile'] != '') {

                // get fcefilename without .html
            $replacementTypeName = str_replace('.html', '', $row['tx_fed_fcefile']);
                // get TCA for type "fcefilename"
            $replacementShowItem = $GLOBALS['TCA'][$table]['types'][$replacementTypeName]['showitem'];

                // if replacementShowItem is set
            if (isset($replacementShowItem)) {

                    // Replace original showitem for types fluidcontent_content with replacement
                $GLOBALS['TCA'][$table]['types']['fluidcontent_content']['showitem'] = $replacementShowItem;
            }
        }
    }
}
?>
