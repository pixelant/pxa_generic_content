<?php
namespace Pixelant\PxaGenericContent\UserFunction;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Pixelant AB
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use \TYPO3\CMS\Core\Utility\PathUtility;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Core\Utility\File\BasicFileUtility;
use \TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;

/**
 * Renders
 *
 * @package	PxaGenericContent
 * @subpackage UserFunction
 */
class DynamicIcons {

    /**
     * Array to store typoscript configuration for pxa_generic_content
     * @var array
     */
    protected $typoScriptSetup;

    /**
     * Array to store messages
     * @var array
     */
    protected $messages;

    /**
     * Array to store filenames (all absolute in array)
     * @var array
     */
    protected $resourceFiles;

    /**
     * Table output from json
     * @var string
     */
    protected $tableOutput;

    /**
     * Array to store parsed icons for selectbox
     * @var array
     */
    protected $selItems;

	/**
	 * @param array $parameters
	 * @param \TYPO3\CMS\Backend\Form\FormEngine $pObj
	 * @return string
	 */
	public function renderField(array &$parameters,\TYPO3\CMS\Backend\Form\FormEngine &$pObj) {

        //unset($parameters['fieldChangeFunc']);

            // get typoscript configuration
        $this->getTypoScriptConf();
            // validate resources
        if (!$this->getAndValidateResourceFiles()) {
            return $this->getMessages();
        }
            // Adds out js and css to form
        $this->addStylesAndJavascriptToForm($pObj);

            // Creates the table and fills selIcons
        $this->parseIconSetJsonFile($parameters);

            // return Selectbox and table html
        return $this->getFormInput($pObj, $parameters) . $this->tableOutput;
	}

    /**
     * Fetch TypoScript setup and store in property
     *
     */
    protected function getTypoScriptConf() {
            // Create ObjectManager
        $this->objectManager = GeneralUtility::makeInstance('Tx_Extbase_Object_ObjectManager');
            // Configuration manager
        $configurationManager = $this->objectManager->get('\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface');
            // full typoscript (couldn't get Tx_Extbase_Configuration_ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS to work....)
        $typoScriptSetupFull = $configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);
            // add settings to array with the settings we need
        $this->typoScriptSetup = $typoScriptSetupFull['plugin.']['tx_pxagenericcontent.']['settings.']['iconSet.'];
            // unset and unset all
        unset($typoScriptSetupFull);
    }

    /**
     * Adds and validates resource files.
     *
     * @return bool Returns true if resource files where added and validated.
     */
    protected function getAndValidateResourceFiles() {
        $validated = true;
            // Add and check path to "iconSet" folder, use getFileAbsFileName to get EXT: resolved.
        $this->resourceFiles['iconSetFolder'] = GeneralUtility::getFileAbsFileName($this->typoScriptSetup['sourceFolder']);
        if (!file_exists($this->resourceFiles['iconSetFolder'])) {
            $validated = false;
            $this->messages[] = 'The setting for icon-set path is not set up, so no icons are available.';
            $this->messages[] = ' - Typoscript: plugin.tx_pxagenericcontent.settings.iconSet.sourceFolder = ' . $this->typoScriptSetup['sourceFolder'];
        }
            // Add anc check iconSet css file
        $this->resourceFiles['iconSetCssFile'] = BasicFileUtility::slashPath($this->resourceFiles['iconSetFolder']) . $this->typoScriptSetup['fileNameCss'];
        if (!file_exists($this->resourceFiles['iconSetCssFile'])) {
            $validated = false;
            $this->messages[] = 'Cannot find the css file for icon-set.';
            $this->messages[] = ' - Typoscript: plugin.tx_pxagenericcontent.settings.iconSet.fileNameCss = ' . $this->typoScriptSetup['fileNameCss'];
            $this->messages[] = ' - (' . $this->resourceFiles['iconSetCssFile'] . ')';
        } else {
                // Font file should be in same folder as css.
            $iconSetCssFileContent = GeneralUtility::getUrl($this->resourceFiles['iconSetCssFile']);
            $matches = false;
            preg_match_all('#(url\(.*/)#', $iconSetCssFileContent, $matches);
            if ( count($matches[0]) > 0 ) {
                $validated = false;
                $this->messages[] = 'Url:s in icon css shouldn\'t point to a subfolder.';
                foreach ($matches[0] as $key => $value) {
                    # code...
                    $this->messages[] = ' - ' . $value;
                }
            }
                // Store iconSetCssFile as a relative file if it was valid
            $this->resourceFiles['iconSetCssFile'] = PathUtility::getRelativePathTo(BasicFileUtility::slashPath($this->resourceFiles['iconSetFolder'])) . $this->typoScriptSetup['fileNameCss'];
        }
            // Add anc check iconSet json file
        $this->resourceFiles['iconSetJsonFile'] = BasicFileUtility::slashPath($this->resourceFiles['iconSetFolder']) . $this->typoScriptSetup['fileNameJson'];
        if (!file_exists($this->resourceFiles['iconSetJsonFile'])) {
            $validated = false;
            $this->messages[] = 'Cannot find the json file for icon-set.';
            $this->messages[] = ' - Typoscript: plugin.tx_pxagenericcontent.settings.iconSet.iconSetJsonFile = ' . $this->typoScriptSetup['fileNameJson'];
            $this->messages[] = ' - (' . $this->resourceFiles['iconSetJsonFile'] . ')';
        }
            // Add simpler message if not Admin
        if ( $GLOBALS['BE_USER']->user['admin'] == 0 && $validated == false) {
            unset($this->messages);
            $this->messages[] = 'There isn\'t any icon-set configured, so no icons are available. Ask administrator to set up an icon-set.';
        }
        return $validated;
    }

    /**
     * Adds and css and javascript to BE form.
     *
     * @param \TYPO3\CMS\Backend\Form\FormEngine $pObj
     * @return void
     */
    protected function addStylesAndJavascriptToForm(\TYPO3\CMS\Backend\Form\FormEngine &$pObj) {
        $iconSetCssRel = $this->resourceFiles['iconSetCssFile'];
        $extCssRel = ExtensionManagementUtility::extRelPath('pxa_generic_content') . 'Resources/Public/Css/BE/dynamicIcons.css';
        $extJsRel = ExtensionManagementUtility::extRelPath('pxa_generic_content') . 'Resources/Public/Js/BE/dynamicIcons.js';
            // Add stylesheets and javascript to BE form
        $pObj->additionalCode_pre = array(
            'iconSetStyles' => '<link rel="stylesheet" href="' . $iconSetCssRel . '" />',
            'dyniconStyles' => '<link rel="stylesheet" href="' . $extCssRel . '" />',
            'dyniconJs' => '<script src="' . $extJsRel . '" type="text/javascript"></script>',
        );
    }

    /**
     * Returns messages as html
     *
     * @return string Returns messages as html.
     */
    protected function getMessages() {
        $output = '';
        if (count($this->messages) > 0) {
            $output = '<ul class="messages">';
            foreach ($this->messages as $key => $message) {
                $output .= '<li>' . $message . '</li>';
            }
            $output .= '</ul>';
        }
        return $output;
    }

    /**
     * Returns table as html
     *
     * @param array $parameters The parameters array
     * @return void
     */
    protected function parseIconSetJsonFile($parameters) {

            // Fetch json data from iconSet
        $jsonData = json_decode(file_get_contents($this->resourceFiles['iconSetJsonFile']));

            // Generate "icon" table output
        $this->tableOutput .= '<table name="table-' . $parameters['itemFormElName'] . '" class="typo3-TCEforms-selectIcons" cellspacing="0" cellpadding="0" border="0">';
        $this->tableOutput .= '<tbody>';

            // Params for table
        $tableColumnsPerRow = (empty($this->typoScriptSetup['columns']) === false) ? (integer)$this->typoScriptSetup['columns'] : 14;
        $tableColumnCurrent = 0;
        $fontSize = (empty($this->typoScriptSetup['fontSize']) === false) ? $this->typoScriptSetup['fontSize'] : '14px';
        $uid = $parameters['row']['uid'];

            // Array to fill with items from json
        $selItems = false;

        foreach ($jsonData->icons as $key => $icon) {
                // Create new row if max columns reached
            if ($tableColumnCurrent == $tableColumnsPerRow) {
                $tableColumnCurrent = 0;
                $this->tableOutput .= '</tr><tr>';
            }

            $this->selItems[] = array($icon->properties->name, $icon->properties->name, NULL);

            $aTagStart = '<a title="' . $icon->properties->name . '" onclick="updateSelection(' . $key . ',\'' . $parameters['itemFormElName'] . '\', \'' . $parameters['table'] . '\', \'' . $uid . '\', \'' . $parameters['field'] . '\',\'' . $icon->properties->name . '\'); this.blur(); return false;" href="#">';
            $this->tableOutput .= ( $parameters['itemFormElValue'] == $icon->properties->name ) ? '<td id="selIcon-' . $key . '" class="current">' : '<td id="selIcon-' . $key . '" >';
            $this->tableOutput .= $aTagStart . '<i style="font-size:' . $fontSize . ';" class="icon-' . $icon->properties->name . '"></i></td></a>';

            $tableColumnCurrent++;
        }

            //Add empty <td>'s to even up the amount of cells in a row:
        while ($tableColumnCurrent == $tableColumnsPerRow) {
            $this->tableOutput .= '<td class="empty">&nbsp;</td>';
            $tableColumnCurrent++;
        }

        $this->tableOutput .= '</tbody>';
        $this->tableOutput .= '</table>';
    }

    /**
     * Returns the html output for the input
     *
     * @param \TYPO3\CMS\Backend\Form\FormEngine $pObj
     * @param array $parameters The parameters array
     * @return string
     */
    protected function getFormInput(\TYPO3\CMS\Backend\Form\FormEngine &$pObj, $parameters) {
        $output = '<div class="dynamicIconSelector">';
        $output .= $pObj->getSingleField_typeInput(
            $parameters['table'],
            $parameters['field'],
            $parameters['row'],
            $parameters
        );
        $output .= '</div>';
        return $output;
    }
}
?>
