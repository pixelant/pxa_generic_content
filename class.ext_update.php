<?php

use \TYPO3\CMS\Core\Utility\GeneralUtility;

class ext_update {

	public function access() {
		return true;
	}

	public function main() {
		if (t3lib_div::_POST('updateSubmit') != '') {
			$content = $this->execUpdates();
			// $content.= 'Update finished successfully.';
		}  else {
			$content = $this->prompt();
		}
		return $content;
	}

	private function getUpdateMappings() {

		$retval = array();

		// Add extension, filename to be converted to pxa_fluidcontent + new filename
		$retval[] = $this->getUpdateMappingItem('pxa_generic_content','accordion.html','Accordion.html',1);
		$retval[] = $this->getUpdateMappingItem('pxa_generic_content','advGrid.html','AdvGrid.html',1);
		$retval[] = $this->getUpdateMappingItem('pxa_generic_content','carousel.html','Carousel.html',1);
		$retval[] = $this->getUpdateMappingItem('pxa_generic_content','contact.html','Contact.html',1);
		$retval[] = $this->getUpdateMappingItem('pxa_generic_content','grid.html','Grid.html',1);
		$retval[] = $this->getUpdateMappingItem('pxa_generic_content','grid2columns.html','Grid2columns.html',1);
		$retval[] = $this->getUpdateMappingItem('pxa_generic_content','grid3columns.html','Grid3columns.html',1);
		$retval[] = $this->getUpdateMappingItem('pxa_generic_content','grid4columns.html','Grid4columns.html',1);
		$retval[] = $this->getUpdateMappingItem('pxa_generic_content','logo.html','Logo.html',1);
		$retval[] = $this->getUpdateMappingItem('pxa_generic_content','logoCarousel.html','LogoCarousel.html',1);
		$retval[] = $this->getUpdateMappingItem('pxa_generic_content','parallax.html','Parallax.html',1);
		$retval[] = $this->getUpdateMappingItem('pxa_generic_content','quote.html','Quote.html',1);
		$retval[] = $this->getUpdateMappingItem('pxa_generic_content','socialIcons.html','SocialIcons.html',1);
		$retval[] = $this->getUpdateMappingItem('pxa_generic_content','tabs.html','Tabs.html',1);
        # new content elements
        $retval[] = $this->getUpdateMappingItem('pxa_generic_content','1.html','1_BigIconTextButton.html',1);
        $retval[] = $this->getUpdateMappingItem('pxa_generic_content','2.html','2_ImageTextLink.html',1);
        $retval[] = $this->getUpdateMappingItem('pxa_generic_content','3.html','3_LeftIconTextButton.html',1);
        $retval[] = $this->getUpdateMappingItem('pxa_generic_content','4.html','4_PageHeader.html',1);
        $retval[] = $this->getUpdateMappingItem('pxa_generic_content','breadcrumbs.html','Breadcrumbs.html',1);
        $retval[] = $this->getUpdateMappingItem('pxa_generic_content','languageMenu.html','LanguageMenu.html',1);
        $retval[] = $this->getUpdateMappingItem('pxa_generic_content','mainNavToggleButton.html','MainNavToggleButton.html',1);
        $retval[] = $this->getUpdateMappingItem('pxa_generic_content','mainNavigation.html','MainNavigation.html',1);
        $retval[] = $this->getUpdateMappingItem('pxa_generic_content','searchForm.html','SearchForm.html',1);
        $retval[] = $this->getUpdateMappingItem('pxa_generic_content','searchToggleButton.html','SearchToggleButton.html',1);
        $retval[] = $this->getUpdateMappingItem('pxa_generic_content','subNavigation.html','SubNavigation.html',1);

		return $retval;

	}

	private function getUpdateMappingTypes() {
		$retval = array();
		$retval['1'] = 'Generic Content - UpperCamelCase';

		return $retval;
	}

	/**
	 * Shows a form to created nested sets data.
	 *
	 * @return	string
	 */
	protected function prompt() {

		$types = $this->getUpdateMappingTypes();
		$updateMappings = $this->getUpdateMappings();

		$content = '';

		$content.= '<form action="' . GeneralUtility::getIndpEnv('REQUEST_URI') . '" method="POST">';
		$content.= '<p>Some utilities to run after update</p>';

		$content.= '<div>' . $this->getTableWithMappings($updateMappings, $types) . '</div>';

		$content.= '<div style="witdh=50%;float:left;">' . $this->getTableWithUsagePerType($updateMappings, $types) . '</div>';

		$content.= '<div style="witdh=50%;float:left;padding-left:25px;">' . $this->getTableWithUsagePerFluidcontent(). '</div>';

		$content.= '<div style="clear:both;margin-bottom:25px;">' . $this->getCheckboxes($types) . '</div>';

		$content.= '<div style="witdh=50%;float:left;"><input type="submit" name="refresh" value="Reload" /></div>';

		$content.= '<div style="witdh=50%;float:right;padding-left:25px;">' .
					'<input type="submit" name="updateSubmit" value="Update" style="color:red;"/><br /><br />' .
					'<input type="checkbox" name="dryRun" id="dryRun" checked="checked" /><label for="dryRun">Only display what would be updated, no actual update!</label>' .
					'</div>';

		$content .= '</form>';

		return $content;
	}

	protected function execUpdates() {

		$updateType = GeneralUtility::_POST('uptateType');
		$dryRun = GeneralUtility::_POST('dryRun');

		$types = $this->getUpdateMappingTypes();
		$updateMappings = $this->getUpdateMappings();

		$whereInString = '';
		if ( $updateType  > 0 ) {

			$content = "<h3>RUNNNING " . (!$dryRun ? " UPDATES" : " DRY RUN") . " FOR " . strtoupper($types[$updateType]) . " (TYPE " . $updateType . ")</h3>";

			$whereInString = $this->getWhereInByType($updateMappings,$updateType);

			$rows = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('uid,tx_fed_fcefile,pi_flexform', 'tt_content', "tx_fed_fcefile in (" . $whereInString . ")");
			$count = 0;
			$undoScript = '';

			$content.= '<table>';
			$content.= '<tr><th>Id</th><th>From</th><th>To</th><th>Status</th></tr>';

			foreach ($rows as $row) {
				$status = false;
				$uid = intval($row['uid']);
				$newFceFile = $this->getToTxFedFceFile($updateMappings, $row['tx_fed_fcefile'], $updateType);
				$pi_flexform = $this->getCheckPiFlexform($row['tx_fed_fcefile'], $newFceFile, $row['pi_flexform']);
				$updatedFlexform = $pi_flexform != $row['pi_flexform'] ? ', pi_flexform updated' : '';

				$content.= '<tr>';
				$content.= $this->getTableCellHtml($uid);
				$content.= $this->getTableCellHtml($row['tx_fed_fcefile']);
				$content.= $this->getTableCellHtml($newFceFile);

				if (strlen($newFceFile) > 0) {
					if (!$dryRun) {
						$status = $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
							'tt_content',
							"uid='" . $uid . "'",
							array(
								'tx_fed_fcefile' => $newFceFile,
								'pi_flexform' => $pi_flexform
							)
						);
						if ($status) {
							$count++;
							$content.= $this->getTableCellHtml('tx_fed_fcefile updated' . $updatedFlexform);
							$undoScript.= "UPDATE tt_content SET tx_fed_fcefile = '" . $row['tx_fed_fcefile'] . "'";
							$undoScript.= $pi_flexform != $row['pi_flexform'] ? ", pi_flexform = '" . $row['pi_flexform'] . "'" : "";
							$undoScript.= " WHERE uid = " . $uid . ";\n";
						} else {
							$content.= $this->getTableCellWarningHtml('Error: ' . $GLOBALS['TYPO3_DB']->sql_error());
						}
					} else {
						$content.= $this->getTableCellHtml('Only view, "dry run", ' . $updatedFlexform);
					}
				} else {
					$content.= $this->getTableCellWarningHtml('No mapping!');
				}
				$content.='</tr>';
			}

			$content.= '</table><br /><br />';
			$content.= '<h3>DONE, ' . $count . ' OF ' . count($rows) . ' ROWS WHERE UPDATED!' . '</h3>';

			if (!$dryRun) {
				$content.= '<h3>UDNO DATA:</h3>';
				$content.= '<textarea cols="100" rows="10">' . $undoScript . '</textarea>';
			}
		} else {
			$content .= '<p style="color:red;">No update-type selected!</p>';
		}

		$content .= '<form action="' . GeneralUtility::getIndpEnv('REQUEST_URI') . '" method="POST">' .
					'<input type="submit" name="mainpage" value="Back" /></form>';


		return $content;

	}

	private function getWhereInByType($updateMappings,$type = 0) {
		$retval = '';
		foreach ($updateMappings as $updateMapping) {
			if ($type == $updateMapping['type'] || $type == 0) {
				if (strlen($retval) > 0) {
					$retval.=",'" . $updateMapping['from_tx_fed_fcefile'] . "'";
				} else {
					$retval.=  "'" . $updateMapping['from_tx_fed_fcefile'] . "'";
				}
			}
		}
		return $retval;
	}

	private function getToTxFedFceFile($updateMappings,$fromTxFedFcefile,$updateType) {
		foreach ($updateMappings as $updateMapping) {
			if ($updateMapping['type'] == $updateType && $fromTxFedFcefile == $updateMapping['from_tx_fed_fcefile']) {
				return $updateMapping['to_tx_fed_fcefile'];
			}
		}
	}

	private function getTableCellHtml($value) {
		return '<td style="padding:2px 10px 2px 0px; border-bottom:solid 1px #e3e3e3;">' . $value . '</td>';
	}

	private function getTableCellWarningHtml($value) {
		return '<td style="padding:2px 10px 2px 0px; color:red; border-bottom:solid 1px #e3e3e3;">' . $value . '</td>';
	}

	private function getCheckboxes($types) {

		$content = '<h3>SELECT WHAT TO UPDATE</h3>';
		$content.= '<p style="padding-bottom:5px;font-size:10px;">You should probably start with the site specific type updates. FluidContent can also match f.ex. pxa_generic_content.</p>';
		$content.= '<select name="uptateType">';
		$content .= '<option value="0" selected="selected">SELECT A TYPE</option>';
		foreach ($types as $type => $name) {
			$content .= '<option value="' . $type . '">' . $name . '</option>';
		}
		$content.= '</select>';
		return $content;
	}

	private function getTableWithUsagePerFluidcontent() {

		$content= '<h3>CURRENT USAGE</h3>';
		$rows = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
			'tx_fed_fcefile as tx_fed_fcefile, count(tx_fed_fcefile) as count',
			'tt_content',
			"tx_fed_fcefile != '' AND deleted = 0",
			"tx_fed_fcefile",
			"tx_fed_fcefile"
		);
		$content.= '<table>';
		$content.= '<tr><th>Type</th><th>Usage</th></tr>';
		$totalCount = 0;
		foreach ($rows as $row) {
			$content.= '<tr>' . $this->getTableCellHtml($row['tx_fed_fcefile']) . $this->getTableCellHtml($row['count']) . '</tr>';
			$totalCount+= intval($row['count']);
		}
		$content.= '<tr>' . $this->getTableCellWarningHtml('(Deleted records are NOT included)') . $this->getTableCellHtml('<b>' . $totalCount . '</b>') . '</tr>';
		$content.= '</table><br /><br />';

		return $content;
	}

	private function getTableWithMappings($updateMappings, $types) {

		$content= '<h3>THIS SCRIPT CAN UPDATE TYPE IN FIELD tx_fed_fcefile FOR FOLLOWING TYPES:</h3>';
		$content.= '<table>';
		$content.= '<tr><th>From</th><th>To</th><th>Type</th></tr>';
		foreach ($updateMappings as $updateMapping) {
			$content.= '<tr>' . $this->getTableCellHtml($updateMapping['from_tx_fed_fcefile']) . $this->getTableCellHtml($updateMapping['to_tx_fed_fcefile']) . $this->getTableCellHtml($types[$updateMapping['type']]) . '</tr>';
		}
		$content.= '</table><br /><br />';
		return $content;
	}

	private function getTableWithUsagePerType($updateMappings,$types) {

		// Display count by type (begin with all "pxa_fluidcontent")
		$content= '<h3>MATCHES BY TYPE</h3>';
		$content.= '<table>';
		$content.= '<tr><th>Type</th><th>Name</th><th>Matches</th></tr>';
		$rows = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
			'COUNT(*) as total',
			'tt_content',
			"tx_fed_fcefile like 'pxa_generic_content:%'"
		);
		$content.= '<tr>' . $this->getTableCellHtml('-') . $this->getTableCellHtml('pxa_generic_content') . $this->getTableCellHtml($rows[0]['total']) . '</tr>';

		foreach ($types as $type => $name) {
			$rows = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
				'COUNT(*) as total',
				'tt_content',
				"tx_fed_fcefile in (" . $this->getWhereInByType($updateMappings,$type) . ") "
			);
			$content.= '<tr>' . $this->getTableCellHtml($type) . $this->getTableCellHtml($name) . $this->getTableCellHtml(intval($rows[0]['total'])) . '</tr>';

		}
		$content.= '<tr>' . $this->getTableCellHtml('') . $this->getTableCellWarningHtml('Can match more than one type<br />Deleted records are included') . $this->getTableCellHtml('') . '</tr>';
		$content.= '</table>';

		return $content;
	}

	private function getUpdateMappingItem($fromExtension, $fromFilename, $toFilename, $type, $toExtension = 'pxa_generic_content') {
		$retval = array();

		$retval['from_tx_fed_fcefile'] = $fromExtension . ":" . $fromFilename;
		$retval['to_tx_fed_fcefile'] = $toExtension . ":" . $toFilename;

		$retval['type'] = $type;
		// Maybee move pi_flexform check here if needs to be more flexible

		return $retval;
	}

		private function getCheckPiFlexform($fromTxFedFcefile, $toTxFedFceFile, $pi_flexform) {

		$retval = $pi_flexform;

		if ($fromTxFedFcefile == "pxa_generic_content:HeaderTextButton.html" && $toTxFedFceFile == "pxa_fluidcontent:9.html") {
			// Field changed from selectbox to input field, and previuosly icon- was added in FE rendering, but not anymore.
			// Add string "icon-" in pi_flexform field icon.
			$flexformTools = GeneralUtility::makeInstance('t3lib_flexformtools');
			$xmlArray = GeneralUtility::xml2array($pi_flexform);

			if (is_array($xmlArray) && isset($xmlArray['data'])) {
				$icon_name = $xmlArray['data']['options']['lDEF']['icon']['vDEF'];
				if(stristr($icon_name, 'icon-') === FALSE) {
					$icon_name = 'icon-' . $icon_name;
					$xmlArray['data']['options']['lDEF']['icon']['vDEF'] = $icon_name;
					$retval = $flexformTools->flexArray2Xml($xmlArray);
				}
			}
		}

		return $retval;
	}
}
?>
