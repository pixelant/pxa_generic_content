<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

    // Custom palettes for fluidcontent_content special types.

    // Header with header, layout and position
$GLOBALS['TCA']['tt_content']['palettes']['fluid_content_header_1'] = array(
    'canNotCollapse' => 1,
    'showitem' => 'header;LLL:EXT:cms/locallang_ttc.xlf:header_formlabel, --linebreak--, header_layout;LLL:EXT:cms/locallang_ttc.xlf:header_layout_formlabel, header_position;LLL:EXT:cms/locallang_ttc.xlf:header_position_formlabel,',
);

    // Header with header, layout, position and subheader
$GLOBALS['TCA']['tt_content']['palettes']['fluid_content_header_2'] = array(
    'canNotCollapse' => 1,
    'showitem' => 'header;LLL:EXT:cms/locallang_ttc.xlf:header_formlabel, --linebreak--, header_layout;LLL:EXT:cms/locallang_ttc.xlf:header_layout_formlabel, header_position;LLL:EXT:cms/locallang_ttc.xlf:header_position_formlabel, subheader;LLL:EXT:cms/locallang_ttc.xlf:subheader_formlabel,',
);

$GLOBALS['TCA']['tt_content']['palettes']['image_settings_generic_content'] = array(
    'canNotCollapse' => 1,
    'showitem' => 'imagewidth;LLL:EXT:cms/locallang_ttc.xlf:imagewidth_formlabel',
);

    /*
        Custom types, set by hook Pixelant\PxaGenericContent\Hook\FormEngine
        when CType = fluidcontent_content AND field tx_fed_fcefile isn't empty.
        Checks if there is a "types" array with same name as in field "tx_fed_fcefile", without .html part of field
        example: $GLOBALS['TCA']['tt_content']['types']["extensionname:filename"]['showitem']
        Will then replace the types "fluidcontent_content" in form so we can have custom forms for different fluidcontents.
    */
$GLOBALS['TCA']['tt_content']['types']['pxa_generic_content:2_ImageTextLink']['showitem'] = '
--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.general;general,
--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.header;fluid_content_header_1,
--linebreak--;LLL:EXT:cms/locallang_ttc.xlf:header.ALT.html_formlabel, bodytext,
tx_fed_fcefile, pi_flexform,
--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.images, image,
--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.appearance,
--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.frames;frames,
--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.image_settings;image_settings_generic_content,
--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access,
--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.visibility;visibility,
tx_pxabootstrap_responsive_utility_class;;;;1-1-1,
--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.access;access,
--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.extended,
--div--;LLL:EXT:lang/locallang_tca.xlf:sys_category.tabs.category, categories,
';

$GLOBALS['TCA']['tt_content']['types']['pxa_generic_content:1_BigIconTextButton']['showitem'] = '
--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.general;general,
--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.header;fluid_content_header_1,
--linebreak--;LLL:EXT:cms/locallang_ttc.xlf:header.ALT.html_formlabel, bodytext,
tx_fed_fcefile, pi_flexform,
--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.appearance,
--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.frames;frames,
--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access,
--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.visibility;visibility,
tx_pxabootstrap_responsive_utility_class;;;;1-1-1,
--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.access;access,
--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.extended,
--div--;LLL:EXT:lang/locallang_tca.xlf:sys_category.tabs.category, categories,
';

$GLOBALS['TCA']['tt_content']['types']['pxa_generic_content:Accordion']['showitem'] = '
--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.general;general,
header;LLL:EXT:pxa_generic_content/Resources/Private/Language/locallang.xlf:labels.formheader.header_notvisible,
tx_fed_fcefile, pi_flexform,
--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.appearance,
--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.frames;frames,
--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access,
--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.visibility;visibility,
tx_pxabootstrap_responsive_utility_class;;;;1-1-1,
--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.access;access,
--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.extended,
--div--;LLL:EXT:lang/locallang_tca.xlf:sys_category.tabs.category, categories,
';

$GLOBALS['TCA']['tt_content']['types']['pxa_generic_content:Logo']['showitem'] = '
--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.general;general,
header;LLL:EXT:pxa_generic_content/Resources/Private/Language/locallang.xlf:labels.formheader.header_notvisible,
tx_fed_fcefile, pi_flexform,
--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.images, image,
--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.appearance,
--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.frames;frames,
--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access,
--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.visibility;visibility,
tx_pxabootstrap_responsive_utility_class;;;;1-1-1,
--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.access;access,
--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.extended,
--div--;LLL:EXT:lang/locallang_tca.xlf:sys_category.tabs.category, categories,
';

$GLOBALS['TCA']['tt_content']['types']['pxa_generic_content:LogoCarousel']['showitem'] = '
--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.general;general,
--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.header;fluid_content_header_1,
tx_fed_fcefile, pi_flexform,
--div--;LLL:EXT:pxa_generic_content/Resources/Private/Language/locallang.xlf:labels.formheader.image_aslogo, image,
--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.appearance,
--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.frames;frames,
--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access,
--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.visibility;visibility,
tx_pxabootstrap_responsive_utility_class;;;;1-1-1,
--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.access;access,
--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.extended,
--div--;LLL:EXT:lang/locallang_tca.xlf:sys_category.tabs.category, categories,
';

$GLOBALS['TCA']['tt_content']['types']['pxa_generic_content:Parallax']['showitem'] = '
--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.general;general,
header;LLL:EXT:pxa_generic_content/Resources/Private/Language/locallang.xlf:labels.formheader.header_notvisible,
tx_fed_fcefile, pi_flexform,
--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.images, image,
--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.appearance,
--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.frames;frames,
--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access,
--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.visibility;visibility,
tx_pxabootstrap_responsive_utility_class;;;;1-1-1,
--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.access;access,
--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.extended,
--div--;LLL:EXT:lang/locallang_tca.xlf:sys_category.tabs.category, categories,
';

$GLOBALS['TCA']['tt_content']['types']['pxa_generic_content:Tabs']['showitem'] = $GLOBALS['TCA']['tt_content']['types']['pxa_generic_content:Accordion']['showitem'];
$GLOBALS['TCA']['tt_content']['types']['pxa_generic_content:AdvGrid']['showitem'] = $GLOBALS['TCA']['tt_content']['types']['pxa_generic_content:Accordion']['showitem'];
$GLOBALS['TCA']['tt_content']['types']['pxa_generic_content:Carousel']['showitem'] = $GLOBALS['TCA']['tt_content']['types']['pxa_generic_content:Accordion']['showitem'];
$GLOBALS['TCA']['tt_content']['types']['pxa_generic_content:Contact']['showitem'] = $GLOBALS['TCA']['tt_content']['types']['pxa_generic_content:Accordion']['showitem'];
$GLOBALS['TCA']['tt_content']['types']['pxa_generic_content:Grid']['showitem'] = $GLOBALS['TCA']['tt_content']['types']['pxa_generic_content:Accordion']['showitem'];
$GLOBALS['TCA']['tt_content']['types']['pxa_generic_content:Quote']['showitem'] = $GLOBALS['TCA']['tt_content']['types']['pxa_generic_content:Accordion']['showitem'];
$GLOBALS['TCA']['tt_content']['types']['pxa_generic_content:SocialIcons']['showitem'] = $GLOBALS['TCA']['tt_content']['types']['pxa_generic_content:Accordion']['showitem'];
$GLOBALS['TCA']['tt_content']['types']['pxa_generic_content:Grid2columns']['showitem'] = $GLOBALS['TCA']['tt_content']['types']['pxa_generic_content:Accordion']['showitem'];
$GLOBALS['TCA']['tt_content']['types']['pxa_generic_content:Grid3columns']['showitem'] = $GLOBALS['TCA']['tt_content']['types']['pxa_generic_content:Accordion']['showitem'];
$GLOBALS['TCA']['tt_content']['types']['pxa_generic_content:Grid4columns']['showitem'] = $GLOBALS['TCA']['tt_content']['types']['pxa_generic_content:Accordion']['showitem'];
$GLOBALS['TCA']['tt_content']['types']['pxa_generic_content:3_LeftIconTextButton']['showitem'] = $GLOBALS['TCA']['tt_content']['types']['pxa_generic_content:1_BigIconTextButton']['showitem'];
