tx_pxagenericcontent.config {
    # cat=pxa_generic_content: //250; type=string; label=Language menu: Default Language Label
    defaultLanguageLabel                = English

    # cat=pxa_generic_content: //251; type=string; label=Language menu: Default Iso Flag
    defaultIsoFlag                      = gb

    # cat=pxa_generic_content: //252; type=boolean; label=Language menu: Hide Non Translated
    hideNotTranslated                   = 1
}
